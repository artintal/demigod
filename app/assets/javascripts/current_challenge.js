/* Sets the progress percentage of the current challenge progress bar
 * based on the passed-in values (as progVal).
 */
var progressValue;

/*
 * Called to set the progress bar percentage value.
 * Will be used when loading previous progress from user history
 * and when updating the progress when "Success" button is
 * clicked.
 */
function setProgressValue(progVal){
	progressValue = progVal;
}

/*
 * For testing of progress bar only. REMOVE LATER.
 * */
function genRandomNum(){
	progressValue = Math.floor(Math.random()*100+1);
}

/*
 * Searches the document for an element named '#successBtn'
 * and attaches the on click event handler to it. This
 * allows the event handler to be "reattached" to the
 * button even when the user clicks a link to redirect
 * back to this page.
 */
/*$(document).on('click', '#successBtn',function(){
	var $bar = $('.progress-bar');
  	var incr = progressValue;
  	
  	//By setting the minimum width of the progress bar when the
  	//value of the bar is less than 3%, the text will always
  	//be visible on the background of the bar, even when the
  	//percentage is extremely low.
  	if(incr <= 3){
  		$bar.css('min-width', '20px');
  	}
  	else{
  		$bar.css('min-width', '0px');
  	}
  	$bar.css('width',incr+'%');
  	$bar.html(incr+'%');
});*/
