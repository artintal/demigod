class ChallengesController < ApplicationController
  before_action :set_challenge, only: [:show, :edit, :update, :destroy, :take_challenge]
    
  # Static description page (nutritional category)
  def nutritional_des
  end
  # Static description page (physical category)
  def physical_des
  end
  # Static description page (intellectual category)
  def intellectual_des
  end

  # GET /challenges
  # GET /challenges.json
  def index
    @physical_challenges = Challenge.uncompleted_challenges(current_user, "Physical")
    @nutritional_challenges = Challenge.uncompleted_challenges(current_user, "Nutritional")
    @intellectual_challenges = Challenge.uncompleted_challenges(current_user, "Intellectual")
    @completed_physical_challenges = Challenge.completed_challenges(current_user, "Physical")
    @completed_nutritional_challenges = Challenge.completed_challenges(current_user, "Nutritional")
    @completed_intellectual_challenges = Challenge.completed_challenges(current_user, "Intellectual")
  end

  # GET /challenges/1
  # GET /challenges/1.json
  def show
    #Render a seperate layout file for showing it in the modal
    render layout: "modal"
  end

  def take_challenge
    @uc = UserChallenge.where(user_id: current_user.id, challenge_id: @challenge.id, progress: "0", checkins: "0").first_or_create
    @history = History.where(user_id: current_user.id, user_challenge_id: @uc.id, progress: "0").first_or_create
    
    #Get the total number of weeks for this challenge
    weeks = @challenge.total_weeks
    
    #Set the start time for this challenge
    start_time = Time.now
    
    #Create a new Week for the challenge. This will be used later
    #to determine if the user is completing their check-ins in a timely manner.
    Week.where(user_id: current_user.id, user_challenge_id: @uc.id, checkins_completed: false, 
      start_week: start_time, end_week: start_time + 7.day, checkins: "0").create  

    
    redirect_to user_path(current_user), notice: "You are now taking the challenge"
  end

  # GET /challenges/new
  def new
    @challenge = Challenge.new
  end

  # GET /challenges/1/edit
  def edit
  end
  
  def challenge_type_index
    @challenge_type = params[:challenge_type] 
  end

  # POST /challenges
  # POST /challenges.json
  def create
    @challenge = Challenge.new(challenge_params.merge(deleted: false))

    respond_to do |format|
      if @challenge.save
        format.html { redirect_to challenges_path, notice: 'Challenge was successfully created.' }
        format.json { render action: 'show', status: :created, location: @challenge }        
      else
        format.html { redirect_to challenges_path, alert: "Your challenge could not be saved for these reasons: " + @challenge.errors.full_messages.to_sentence }
        format.json { render json: @challenge.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /challenges/1
  # PATCH/PUT /challenges/1.json
  def update
    respond_to do |format|
      if @challenge.update(challenge_params)
        format.html { redirect_to challenges_path, notice: 'Challenge was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @challenge.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /challenges/1
  # DELETE /challenges/1.json
  def destroy
    @challenge.deleted = true
    @challenge.save
    respond_to do |format|
      format.html { redirect_to challenges_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_challenge
      @challenge = Challenge.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def challenge_params
      params.require(:challenge).permit(:parent_id, :user_made, :name, :description, :points, :category, :created_by, :per_week, :total_weeks, :statistic_ids => [])
    end
end
