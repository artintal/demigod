class HomeController < ApplicationController
  def index
    if user_signed_in?
      redirect_to user_path(current_user)
    end
  end

  def nutritional
  end

  def physical
  end

  def intellectual
  end

  def faq
  end

end
