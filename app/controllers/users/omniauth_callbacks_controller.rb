class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def facebook
    @user = User.find_for_facebook_oauth(request.env["omniauth.auth"])
    @user.update_attribute(:auth_token, request.env["omniauth.auth"].credentials.token) if @user.auth_token.blank?
    @user.update_attribute(:provider, request.env["omniauth.auth"].provider) if @user.provider.blank?
    @user.update_attribute(:uid, request.env["omniauth.auth"].uid) if @user.provider.blank?
    if @user.persisted?
      sign_in_and_redirect @user, event: :authentication
      set_flash_message(:notice, :success, kind: "Facebook") if is_navigational_format?
    else
      session["devise.facebook_data"] = request.env["omniauth.auth"]
      redirect_to new_user_registration_url
    end
  end
end
