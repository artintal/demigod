require 'fb_graph'
class UsersController < ApplicationController
  before_filter :authenticate_user!

  def create
    @user = User.create( user_params )
  end
  
  def admin
    authorize! :admin, @user, message: 'Not authorized as an administrator.'
  end
  
  def admin_challenges
    authorize! :admin, @user, message: 'Not authorized as an administrator.'
    @challenges = Challenge.where(user_made: false, deleted: false).to_a
  end

  def index
    authorize! :index, @user, :message => 'Not authorized as an administrator.'
    @users = User.all
  end
 
  def my_challenges
    if current_user.has_role? :admin
      @challenges = Challenge.where(created_by: [current_user.id, nil], deleted: false)
    else
      @challenges = Challenge.where(created_by: current_user.id, deleted: false)
    end
  end

  def completed_challenges
    @challenges = Challenge.all_completed_challenges(current_user)
  end
  
  def progress
    @user = User.find(params[:id])
    @percent_total = LazyHighCharts::HighChart.new('column') do |f|
      f.title({text: "Your Challenges Completed Compared to All Challenges in Demigod"})
      f.series(type: 'column', name: "Total Challenge Completion", data: [["# of challenges completed", UserChallenge.where(progress: 100, user_id: current_user.id).count]])
      f.series(type: 'column', name: "Total # of challenges in Demigod", data:[["Total # of challenges in Demigod", Challenge.where(deleted: false).count]])
      f.plotOptions(column: {showInLegend: true, dataLabels: {enabled: false}})
      f.colors(["#7ab317","#242424"])
      f.chart({width: "360"})
    end
    @percent_physical = LazyHighCharts::HighChart.new('column') do |f|
      f.title({text: "Percentage of Physical Challenges Completed"})
      f.series(type: 'column', name: "Total Challenge Completion", data: [["# of physical challenges completed", Challenge.completed_challenges(current_user, "Physical").count]])
      f.series(type: 'column', name: "Total # of physical challenges in Demigod", data:[["Total # of physical challenges in Demigod", Challenge.where(category: "Physical", deleted: false).count]])
      f.plotOptions(column: {showInLegend: true, dataLabels: {enabled: false}})
      f.colors(["#7ab317","#242424"])
      f.chart({width: "360"})
    end
    @percent_intellectual = LazyHighCharts::HighChart.new('pie') do |f|
      f.title({text: "Percentage of Intellectual Challenges Completed"})
      f.series(type: 'pie', name: "Total Challenge Completion", data: [["# of intellectual challenges completed", Challenge.completed_challenges(current_user, "Intellectual").count],["Total # of intellectual challenges in Demigod", Challenge.where(category: "Intellectual", deleted: false).count]])
      f.plotOptions(pie: {showInLegend: true, dataLabels: {enabled: false}})
      f.colors(["#7ab317","#242424"])
      f.chart({width: "360"})
    end
    @percent_nutritional = LazyHighCharts::HighChart.new('pie') do |f|
      f.title({text: "Percentage of Nutritional Challenges Completed"})
      f.series(type: 'pie', name: "Total Challenge Completion", data: [["# of nutritional challenges completed", Challenge.completed_challenges(current_user, "Nutritional").count],["Total # of nutritional challenges in Demigod", Challenge.where(category: "Nutritional", deleted: false).count]])
      f.plotOptions(pie: {showInLegend: true, dataLabels: {enabled: false}})
      f.colors(["#7ab317","#242424"])
      f.chart({width: "360"})
    end
    @types_completed = LazyHighCharts::HighChart.new('pie') do |f|
      completed_challenge_ids = UserChallenge.where(progress: 100).map(&:challenge_id)
      completed_challenges = Challenge.where(id: completed_challenge_ids, deleted: false)
      nutritional_count = completed_challenges.where(category: "Nutritional").count
      physical_count = completed_challenges.where(category: "Physical").count
      intellectual_count = completed_challenges.where(category: "Intellectual").count
      f.title({text: "Your Category Breakdown of Completed Challenges"})
      f.subtitle({text: "Try to aim for a balance among all three!"})
      f.series(type: 'pie', name: "counts", data: [["Physical", physical_count],["Nutritional", nutritional_count],["Intellectual", intellectual_count]])
      f.plotOptions(pie: {showInLegend: true, dataLabels: {enabled: false}})
      f.chart({width: "360"})
      f.colors(["#7ab317","#b70101","#242424"])
    end
  end

  def show
    @user = User.find(params[:id])
    @current_challenge = current_user.user_challenges.last

    if @current_challenge.present?
      
      #If the there is no history for this current user challenge, then @current_history
      #will be equal to nil. Otherwise it will return the most recent history entry for
      #this challenge.
      @current_history = @current_challenge.histories.last
      @current_progress = 0
      
      if @current_history.present?
        @current_progress = @current_history.progress   
      end  
        
      
      @challenge = @current_challenge.challenge
      @per_week = @challenge.per_week
      @weeks = @challenge.total_weeks
      @start_timestamp = @current_challenge.histories.first.created_at
      
    end
  end

  def post_to_wall
    user_challenge = UserChallenge.find(params[:challenge_id])
    challenge = user_challenge.challenge
    me = FbGraph::User.me(current_user.auth_token)
    if (progress = user_challenge.progress.to_f) < 100.0
      me.feed!(
        message: "I am #{progress.round(2)}% of the way done with the #{challenge.name} challenge in Demigod!",
        link: "http://www.demigod.me",
        name: "Demigod",
        description: "I am participating in a Demigod challenge to better myself! It's shown that a set schedule of check ins is good at creating beneficial habits. I just wanted to share my progress."
      ) 
    else
      me.feed!(
        message: "I have completed the #{challenge.name} challenge in Demigod!",
        link: "http://www.demigod.me",
        name: "Demigod",
        description: "I have earned #{challenge.points} points on the #{challenge.name} challenge by completing the task: #{challenge.description}."
      )
    end
    redirect_to user_path(current_user), notice: "Your progess has been posted to Facebook"
  end
  
  def edit
    @user = User.find(params[:id])
  end
  
  def update
    @user = User.find(params[:id])
    if current_user.id == @user.id or current_user.has_role? :admin
      if @user.update_attributes(user_params)
        redirect_to user_path, :notice => "User updated."
      else
        redirect_to user_path, :alert => "Unable to update user."
      end
    else
      redirect_to :back, message: "You are not authorized to do that."
    end
    Statistic.all.each do |stat|
      if UserStatistic.where(user_id: @user.id, statistic_id: stat.id).blank?
        UserStatistic.create(user_id: @user.id, statistic_id: stat.id, level: "1", points_until_level_up: "40")
      end
    end
  end
    
  def destroy
    authorize! :destroy, @user, :message => 'Not authorized as an administrator.'
    user = User.find(params[:id])
    unless user == current_user
      user.destroy
      redirect_to users_path, :notice => "User deleted."
    else
      redirect_to users_path, :notice => "Can't delete yourself."
    end
  end
  
  def create_history
    @current_challenge = current_user.user_challenges.last
    current_progress = BigDecimal.new(params[:newProgress])
    finish = false
    
    #Grab the current time so that it can be compared with the Weeks associated with
    #the current user challenge.
    current_time = Time.now
    
    #Find the latest week where the current time falls within the week's starting and ending dates.
    @week = Week.where("start_week <= ? AND end_week >= ?", current_time, current_time).where(user_id: current_user.id).last
    
    @challenge = Challenge.find(@current_challenge.challenge_id)
    
    #If we cannot find a week in the database whose start and end time bracket the current time, then
    #we need to create a new week in the database.
    #raise @week.inspect
    failed_checkin = false   
    if @week.nil?
      #Get the last week for this challenge.
      @last_week = Week.where(user_id: current_user.id, user_challenge_id: @current_challenge.id).last 
      
      #If the user has not completed the required number of checkins for the past week, then we will
      #remove the last week's checkins from the current count of checkins for this user challenge AND
      #we will subtract last week's progress because they will need to redo last week since they failed
      #to do the required number of checkins.
      #if @last_week.end_week < Time.now - 7.day      
       #temp = (100.0/(@challenge.per_week * @challenge.total_weeks))
       #current_progress = current_progress - temp  
                      
       #@current_challenge.update_attribute(:progress, current_progress)
       #failed_checkin = true
      if !@last_week.checkins_completed
        #@last_week = Week.where(user_id: current_user.id, user_challenge.id: @current_challenge.id, checkins_completed: false).first
        
        #Remove the number of checkins for that week.
        @current_challenge.update_attribute(:checkins, 
          @current_challenge.checkins - @last_week.checkins)
        
        #Subtract last week's progress from the current progress value since the user will
        #have to redo the week.
        temp = @last_week.checkins * (100.0/(@challenge.per_week * @challenge.total_weeks))
        current_progress = @current_challenge.progress - temp
        
        @current_challenge.update_attribute(:progress, current_progress)
          
        failed_checkin = true
      end    
      
      #If the user has not failed their checkins for the week, then we create a new week starting immediately when
      #th old week ended. If however, they have failed their checkins, then we create a new week starting now since
      #they will have to redo the week starting now.
      if !failed_checkin
        @week = Week.where(user_id: current_user.id, user_challenge_id: @current_challenge.id, checkins_completed: false, 
          start_week: @last_week.end_week, end_week: @last_week.end_week + 7.day, checkins: "0").create
      else
        @week = Week.where(user_id: current_user.id, user_challenge_id: @current_challenge.id, checkins_completed: false, 
          start_week: current_time, end_week: current_time + 7.day, checkins: "0").create     
      end
          
    end 
    
    
    #If you failed the checkin, then the checkin count for this week will NOT increment. You will
    #have to start over with the next checkin.
    if !failed_checkin
      #increment the per week checkin count.      
      @week.update_attribute(:checkins, @week.checkins + 1)
      
      #Increment the checkin count for the entire current user challenge.
      @current_challenge.update_attribute(:checkins, @current_challenge.checkins + 1) 
      
      #If the current progress is greater than or equal to 100 (because the progress is rounded, the
      #total may end up being slightly higher than 100%), then we set the progress to exactly 100 and
      #set the finished boolean to true.
      if current_progress >= 99.9 
        current_progress = BigDecimal.new("100.0")
        
        #set finish to true once the challenge is complete.
        finish = true    
        
        #Level up happens here
        UserStatistic.perform_level_check(current_user, @current_challenge)    
      end
             
    end
       
    #Find the challenge associated with the current user challenge and do
    #a check to determine if the number of checkins for the week has been completed.

    if @week.checkins == @challenge.per_week
      @week.update_attribute(:checkins_completed, true)
    end   
    
        
    #Create the new history and associate it with the current week.
    @user_history = History.create(user_id: current_user.id, user_challenge_id: @current_challenge.id, week_id: @week.id, progress: current_progress)
    
    #update the progress value and the finished value for the current User
    @current_challenge.update_attributes(progress: current_progress, finished: finish)  
    
    #Return whether or not the user failed one or more checkins.
    render text: failed_checkin
  end
  
  #Returns true if the next checkin can occur (i.e. a 24 hour period has elapsed), false otherwise.
  def can_next_checkin_occur   
    @current_challenge = current_user.user_challenges.last
    
    @curr_week = Week.where(user_id: current_user.id, user_challenge_id: @current_challenge.id).last
    @day_of_week = ((Time.now - @curr_week.start_week)/1.day).ceil
    
    @last_history = @current_challenge.histories.last
    @last_timestamp = @last_history.created_at
    
    #If the user has checked in in the last 24 hours, then they cannot check in again until
    #the full 24 hours has elapsed.
    if (@last_timestamp < @curr_week.start_week + @day_of_week * 1.day) && 
       (@last_timestamp > @curr_week.start_week + (@day_of_week - 1) * 1.day) &&
       (@curr_week.checkins >= 1)
      render text: "false"
    else
      render text: "true"
    end

  end
  
  
  #Get the current progress value from the current user's challenge history (for the challenge)
  #that they are currently taking).
  def get_progress
    @current_challenge = current_user.user_challenges.last
    @last_history = @current_challenge.histories.last
    @last_progress= @last_history.progress
    
    render text: @last_progress
  end
  
  #Uses the past history of the challenge to count the number of times that the user
  #has checked in for their current challenge.
  def get_checkin_count
    @current_challenge = current_user.user_challenges.last
    
    #Subtract 1 from the total number of histories for this current challenge because the first history
    #is created when the user clicks "Take Challenge" and is therefore not part of the checkin count.
    #@checkin_count = History.where(user_id: current_user.id, user_challenge_id: @current_challenge.id).count - 1
    @checkin_count = @current_challenge.checkins
    
    render text: @checkin_count
    
  end
  
  #Returns "true" if the user has completed their required number of check-ins for this week and "false"
  #otherwise.
  def has_user_finished_this_week
    @current_challenge = current_user.user_challenges.last
    @this_week = Week.where(user_id: current_user.id, user_challenge_id: @current_challenge.id).last
    
    #If the user has completed the required number of checkins for this week AND the current time is
    #greater than the end date for the week, then true is returned. Otherwise, false is returned.
    if @this_week.checkins_completed && @this_week.end_week >= Time.now
      render text: "true"
    else
      render text: "false"
    end
    
  end
  
  #Returns True if the current time is during the first day of the current week for the current challenge. 
  #Otherwise, returns False.
  def is_the_first_day_of_the_week
    @current_challenge = current_user.user_challenges.last
    @current_week = Week.where(user_id: current_user.id, user_challenge_id: @current_challenge.id).last
    #Check if the current time is during the first day of the current week
    if Time.now < @current_week.start_week + 1.day

      #Check if it is the first check-in for the curent week
     
      if @current_week.checkins >= 1
        
        render text: "false"
      else
        #First check-in of the current week
        render text: "true"
      end
    else
      #The current is after the first day of the current week
      render text: "false"
    end
  end
  
  #Returns the start date for the next week of the challenge.
  def get_start_of_next_week
    @current_challenge = current_user.user_challenges.last
    @current_week = Week.where(user_id: current_user.id, user_challenge_id: @current_challenge.id).last
    
    #Nicely format the date and convert to this timezone.
    end_date = @current_week.end_week
    #Return the end date for the current week, which is also the start date for the next week.   
    render text: end_date.in_time_zone('Mountain Time (US & Canada)').strftime("%m/%d/%Y at %H:%M:%S %p")
  end
  
  private

  # Use strong_parameters for attribute whitelisting
  # Be sure to update your create() and update() controller methods.
  
  def user_params
    params.require(:user).permit(:avatar, :email)
  end
  
end
