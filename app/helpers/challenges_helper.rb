module ChallengesHelper
  def selects_for_parent
    [["None", ""]] + Challenge.all.map{|c| [c.name, c.id]}
  end
 
  def get_name_by_id(challengeId)
    Challenge.where(id: challengeId).first.name
  end

  def display_parent_select?
    Challenge.all.present?
  end
  
  def selects_for_category
    ["Physical","Nutritional","Intellectual"]
  end
  
  def challenge_type_list(challenge_type, completed_flag)
    if completed_flag
      case challenge_type
      when "Physical"
        @physical_challenges
      when "Nutritional"
        @nutritional_challenges
      when "Intellectual"
        @intellectual_challenges
      end
    else
      case challenge_type
      when "Physical"
        @completed_physical_challenges
      when "Nutritional"
        @completed_nutritional_challenges
      when "Intellectual"
        @completed_intellectual_challenges
      end
    end
  end
      
  def filter_valid_challenges(challenges_list, user)
    if user.has_role? :admin
      challenges_list
    else
      ret = []
      unless challenges_list.empty?
        challenges_list.each do |challenge|
          #if challenge.created_by == user or valid_check(user, challenge)
          if (valid_check(user, challenge) == true)
            ret.push(challenge)
          end
        end
        return ret
      else
        return ret
      end
    end
  end

  def my_challenges(user)
    if user.has_role? :admin
      Challenge.all
    else
      Challenge.where(created_by: user, deleted: false)
    end
  end

  def valid_check(user, challenge)
    if challenge.parent_id
      completed_check(user, Challenge.where(id: challenge.parent_id).first)
    else
      true
    end
  end

  def completed_check(user, challenge)
    @user_challenge = UserChallenge.where(user_id: user, challenge_id: challenge.id, finished: true).first
    if @user_challenge
      true
    else
      false
    end
  end 
  
#Get the user challenge for the given user and challenge, using their IDs to find the
  #user challenge that corresponds to this user and the challenge.
  def user_challenges_list(user, challenge)
    @user_challenge = UserChallenge.where(user_id: user, challenge_id: challenge.id, finished: true)
  end
  

end
