module UsersHelper
  def left_column_stats(user)
    @us = UserStatistic.where(user_id: user.id).all
    half_amount = @us.count/2
    @us[half_amount..-1]
  end

  def right_column_stats(user)
    @us = UserStatistic.where(user_id: user.id).all
    half_amount = @us.count/2
    @us[0..half_amount-1]
  end

  def percentage_of_statistic_completed(user_stat)
    current_user.percentage_completed(user_stat)
  end 
end
