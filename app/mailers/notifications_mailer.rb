class NotificationsMailer < ActionMailer::Base

  default :from => "noreply@demigod.me"
  default :to => "demigodcs460@gmail.com"

  def new_message(message)
    @message = message
    mail(:subject => "[Demigod] #{message.subject}")
  end

end