class Challenge < ActiveRecord::Base
  has_many :user_challenges
  has_many :challenge_statistics
  has_many :statistics, :through => :challenge_statistics

  validates_presence_of :name, :description, :points, :per_week, :total_weeks, :statistic_ids
  
  #Grab the incomplete challenges by subtracting the completed challenges from
  #the entire list
  def self.uncompleted_challenges(user, category)
    all_challenges = self.where(category: category, deleted: false).all
    UserChallenge.where(user_id: user.id, finished: true).each do |uc|
      all_challenges -= [uc.challenge]
    end
    all_challenges 
  end

  def self.completed_challenges(user, category)
    completed_challenges = []
    UserChallenge.where(user_id: user.id, finished: true).each do |uc|
      if uc.challenge.category == category
        completed_challenges += [uc.challenge]
      end
    end
    completed_challenges
  end
  
  def self.all_completed_challenges(user)
    completed_challenges = []
    UserChallenge.where(user_id: user.id, finished:true).each do |uc|
      completed_challenges += [uc.challenge]
    end
    completed_challenges
  end
  
  def self.challengesByType(category)
    self.where(category: category, deleted: false).all
  end
end
