class History < ActiveRecord::Base
  belongs_to :user
  belongs_to :user_challenge
  belongs_to :week
end
