class Statistic < ActiveRecord::Base
  has_many :user_statistics
  has_many :challenge_statistics
  has_many :challenges, :through => :challenge_statistics
end
