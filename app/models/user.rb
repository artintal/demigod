class User < ActiveRecord::Base
  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  devise :omniauthable, omniauth_providers: [:facebook]
  
  before_save :seed_statistics    
         
  has_attached_file :avatar,
    :styles => {:large => "150x150#"}, 
    :default_url => "avatar_blank.png"
  
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
  has_many :user_challenges
  has_many :histories
  has_many :user_statistics

  def self.find_for_facebook_oauth(auth)
    logger.error "*"*60
    logger.error auth.inspect
    logger.error auth.slice(:email).inspect
    where(email: auth.info.email).first_or_create do |user|
      user.provider = auth.provider
      user.uid = auth.uid
      user.email = auth.info.email
      user.password = Devise.friendly_token[0,20]
      user.name = auth.info.name
      user.auth_token = auth.credentials.token
    end
  end
  
  def seed_statistics
    Statistic.all.each do |stat|
      if UserStatistic.where(user_id: id, statistic_id: stat.id).blank?
        UserStatistic.create(user_id: id, statistic_id: stat.id, level: "1", points_until_level_up: "40")
      end
    end
  end
  
  def percentage_completed(user_stat)
    curr_level = user_stat.level
    
    #points to get to the next level.
    exp_amount = 40
    
    if curr_level > 1
      exp_amount = ((curr_level**2+curr_level)/2)*100 - (curr_level*100)
    end

    #number of points the user has until the next level.
    points_left = user_stat.points_until_level_up
    
    #percentage left to complete.
    percent_left = (points_left.to_f/exp_amount.to_f)*100.0
    
    #percentage completed by the user.
    percent_complete = 100.0 - percent_left.to_f

  end
end
