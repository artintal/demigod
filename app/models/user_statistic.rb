class UserStatistic < ActiveRecord::Base
  belongs_to :user
  belongs_to :statistic

  def self.perform_level_check(user, u_challenge)
    points_for_completion = u_challenge.challenge.points
    statistics_affected = u_challenge.challenge.statistics
    statistics_affected.each do |s|
      user_statistic = UserStatistic.where(user_id: user.id, statistic_id: s.id).first
      if ((@new_count = user_statistic.points_until_level_up - points_for_completion) <= 0)
        new_level = user_statistic.level + 1
        user_statistic.update_attribute(:level, new_level)
        @new_count = @new_count.abs
        new_xp_amount = ((new_level**2+new_level)/2)*100 - (new_level*100)
        
        user_statistic.update_attribute(:points_until_level_up, new_xp_amount - @new_count)
        
        if new_xp_amount - @new_count <= 0
          self.perform_level_check(user, u_challenge)
        end
        
      else
        user_statistic.update_attribute(:points_until_level_up, @new_count)
      end
    end
  end
end
