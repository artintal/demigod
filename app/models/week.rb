class Week < ActiveRecord::Base
   belongs_to :user
   belongs_to :user_challenge
   has_many :histories
end
