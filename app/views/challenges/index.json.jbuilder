json.array!(@challenges) do |challenge|
  json.extract! challenge, :id, :parent_id, :user_made, :name, :description, :points, :category
  json.url challenge_url(challenge, format: :json)
end
