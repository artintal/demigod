json.array!(@histories) do |history|
  json.extract! history, :id, :user_id, :user_challenge_id, :progress
  json.url history_url(history, format: :json)
end
