json.array!(@statistics) do |statistic|
  json.extract! statistic, :id, :user_id, :level, :name, :points_until_level_up
  json.url statistic_url(statistic, format: :json)
end
