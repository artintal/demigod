json.array!(@user_challenges) do |user_challenge|
  json.extract! user_challenge, :id, :challenge_id, :end_date, :user_id, :progress, :type
  json.url user_challenge_url(user_challenge, format: :json)
end
