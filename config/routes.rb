Demigod::Application.routes.draw do
  resources :statistics

  resources :histories

  resources :user_challenges

  resources :challenges

  root :to => "home#index"
  get "/admin", to: "users#admin", as: "admin"
  get "/admin_challenges", to: "users#admin_challenges", as: "admin_challenges"
  get "/take_challenge/:id", to: "challenges#take_challenge", as: "take_challenge"
  get "/post_to_wall/:challenge_id", to: "users#post_to_wall", as: "wall_post"
  get "/create_history/:currProgress", to: "users#create_history", as: "create_history"
  get "/can_next_checkin_occur", to: "users#can_next_checkin_occur", as: "can_next_checkin_occur"
  get "/get_checkin_count", to: "users#get_checkin_count", as: "get_checkin_count"
  get "/get_progress", to: "users#get_progress", as: "get_progress"
  get "/has_user_finished_this_week", to: "users#has_user_finished_this_week", as: "has_user_finished_this_week"
  get "/users/:id/progress", to: "users#progress", as: "progress"
  get "/is_the_first_day_of_the_week", to: "users#is_the_first_day_of_the_week", as: "is_the_first_day_of_the_week"
  get "/get_start_of_next_week", to: "users#get_start_of_next_week", as: "get_start_of_next_week"
  get "/my_challenges", to: "users#my_challenges", as: "my_challenges"
  get "/completed_challenges", to: "users#completed_challenges", as: "completed_challenges"
  get "/list/:challenge_type", to: "challenges#challenge_type_index", as: "challenge_type"
  get "/nutritional", to: "home#nutritional", as: "nutritional"
  get "/physical", to: "home#physical", as: "physical"
  get "/intellectual", to: "home#intellectual", as: "intellectual"
  get "/category/nutritional", to: "challenges#nutritional_des", as: "nutritional_des"
  get "/category/physical", to: "challenges#physical_des", as: "physical_des"
  get "/category/intellectual", to: "challenges#intellectual_des", as: "intellectual_des"
  get "/abandon_challenge/:user_id", to: "user_challenges#abandon_challenge", as: "abandon_challenge"
  get "/faq", to: "home#faq", as: "faq"
  match 'contact' => 'contact#new', :as => 'contact', :via => :get
  match 'contact' => 'contact#create', :via => :post
  devise_for :users, :controllers => {omniauth_callbacks: "users/omniauth_callbacks", :registrations => "registrations"}
  resources :users
end
