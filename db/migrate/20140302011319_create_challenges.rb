class CreateChallenges < ActiveRecord::Migration
  def change
    create_table :challenges do |t|
      t.integer :parent_id
      t.boolean :user_made
      t.string :name
      t.text :description
      t.integer :points
      t.string :category

      t.timestamps
    end
  end
end
