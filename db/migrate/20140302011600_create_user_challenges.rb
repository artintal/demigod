class CreateUserChallenges < ActiveRecord::Migration
  def change
    create_table :user_challenges do |t|
      t.integer :challenge_id
      t.datetime :end_date
      t.integer :user_id
      t.integer :progress
      t.string :type

      t.timestamps
    end
  end
end
