class AddCreatedByToChallenge < ActiveRecord::Migration
  def change
    add_column :challenges, :created_by, :integer
  end
end
