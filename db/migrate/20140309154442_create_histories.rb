class CreateHistories < ActiveRecord::Migration
  def change
    create_table :histories do |t|
      t.integer :user_id
      t.integer :user_challenge_id
      t.integer :progress

      t.timestamps
    end
  end
end
