class AddPerWeekAndTotalWeeksToChallenges < ActiveRecord::Migration
  def change
    add_column :challenges, :per_week, :integer
    add_column :challenges, :total_weeks, :integer
  end
end
