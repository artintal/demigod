class CreateStatistics < ActiveRecord::Migration
  def change
    create_table :statistics do |t|
      t.integer :user_id
      t.integer :level
      t.string :name
      t.integer :points_until_level_up

      t.timestamps
    end
  end
end
