class CreateUserStatistics < ActiveRecord::Migration
  def change
    create_table :user_statistics do |t|
      t.integer :user_id
      t.integer :level
      t.integer :points_until_level_up
      t.integer :statistic_id

      t.timestamps
    end
  end
end
