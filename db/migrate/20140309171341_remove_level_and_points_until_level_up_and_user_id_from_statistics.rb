class RemoveLevelAndPointsUntilLevelUpAndUserIdFromStatistics < ActiveRecord::Migration
  def change
    remove_column :statistics, :level, :integer
    remove_column :statistics, :points_until_level_up, :integer
    remove_column :statistics, :user_id, :integer
  end
end
