class AddFinishedToUserChallenge < ActiveRecord::Migration
  def change
    add_column :user_challenges, :finished, :boolean, default: false
  end
end
