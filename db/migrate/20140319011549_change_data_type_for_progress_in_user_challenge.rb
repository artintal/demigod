class ChangeDataTypeForProgressInUserChallenge < ActiveRecord::Migration
  def change
    change_table :user_challenges do |t|
      t.change :progress, :decimal
    end
  end
end
