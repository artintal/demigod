class ChangeDataTypeForProgressInHistory < ActiveRecord::Migration
  def change
    change_table :histories do |t|
      t.change :progress, :decimal
    end
  end
end
