class CreateWeeks < ActiveRecord::Migration
  def change
    create_table :weeks do |t|
      t.datetime :start_week
      t.datetime :end_week
      t.boolean :checkins_completed

      t.timestamps
    end
  end
end
