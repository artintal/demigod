class AddUserIdAndUserChallengeIdToWeek < ActiveRecord::Migration
  def change
    add_column :weeks, :user_id, :integer
    add_column :weeks, :user_challenge_id, :integer
  end
end
