class AddWeekIdToHistory < ActiveRecord::Migration
  def change
    add_column :histories, :week_id, :integer
  end
end
