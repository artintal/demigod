class AddCheckinsToWeek < ActiveRecord::Migration
  def change
    add_column :weeks, :checkins, :integer
  end
end
