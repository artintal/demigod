class CreateChallengeStatistics < ActiveRecord::Migration
  def change
    create_table :challenge_statistics do |t|
      t.integer :challenge_id
      t.integer :statistic_id

      t.timestamps
    end
  end
end
