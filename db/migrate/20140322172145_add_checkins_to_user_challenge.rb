class AddCheckinsToUserChallenge < ActiveRecord::Migration
  def change
    add_column :user_challenges, :checkins, :integer
  end
end
