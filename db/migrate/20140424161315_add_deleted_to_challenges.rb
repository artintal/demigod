class AddDeletedToChallenges < ActiveRecord::Migration
  def change
    add_column :challenges, :deleted, :boolean
  end
end
