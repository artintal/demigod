# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#puts 'ROLES'
#YAML.load(ENV['ROLES']).each do |role|
#  Role.find_or_create_by_name(role)
#  puts 'role: ' << role
#end
#puts 'DEFAULT USERS'
#user = User.find_or_create_by_email :name => ENV['ADMIN_NAME'].dup, :email => ENV['ADMIN_EMAIL'].dup, :password => ENV['ADMIN_PASSWORD'].dup, :password_confirmation => ENV['ADMIN_PASSWORD'].dup
#puts 'user: ' << user.name
#user.add_role :admin

#Make the statistics
["Strength", "Intelligence", "Diet", "Altruism", "Endurance", "Health"].each do |stat_name|
  Statistic.where(name: stat_name).first_or_create
end

#Give all existing users the user statistics if they dont exist already
User.all.each do |user|
  Statistic.all.each do |stat|
    if UserStatistic.where(user_id: user.id, statistic_id: stat.id).blank?
      UserStatistic.create(user_id: user.id, statistic_id: stat.id, level: "1", points_until_level_up: "40")
    end
  end
end

require 'csv'
CSV.foreach('challenges.csv') do |row|
  next if $. == 1
  statls = []
  #Strength
  if row[6]
    statls << Statistic.where(name: "Strength").first.id
  end
  if row[7]
    statls << Statistic.where(name: "Endurance").first.id
  end
  if row[8]
    statls << Statistic.where(name: "Intelligence").first.id
  end
  if row[9]
    statls << Statistic.where(name: "Altruism").first.id
  end
  if row[10]
    statls << Statistic.where(name: "Diet").first.id
  end
  if row[11]
    statls << Statistic.where(name: "Health").first.id
  end
  challenge = Challenge.where(user_made: false, name: row[0], description: row[1], points: row[12].to_i, category: row[2], parent_id: Challenge.where(description: row[3]).first.try(:id), per_week: row[4].to_i, total_weeks: row[5].to_i, created_by: nil, deleted: false).first_or_create
  challenge.statistic_ids = statls
  challenge.save
end
