### UTILITY METHODS ###

def create_visitor
  @visitor ||= { :name => "Testy McUserton", :email => "example@example.com",
    :password => "changeme", :password_confirmation => "changeme" }
end

def find_user
  @user ||= User.first conditions: {:email => @visitor[:email]}
end

def create_unconfirmed_user
  create_visitor
  delete_user
  sign_up
  visit '/users/sign_out'
end

def create_user
  create_visitor
  delete_user
  make_statistics
  @user = FactoryGirl.create(:user, email: @visitor[:email])
end

def make_statistics
  ["Altruism", "Endurance", "Health","Strength","Intelligence","Diet"].each do |stat|
    FactoryGirl.create(:statistic, name: "#{stat}")
  end
end

def delete_user
  @user ||= User.first conditions: {:email => @visitor[:email]}
  @user.destroy unless @user.nil?
end

def sign_up
  delete_user
  visit '/users/sign_up'
  fill_in "Email", :with => @visitor[:email]
  fill_in "user_password", :with => @visitor[:password]
  fill_in "user_password_confirmation", :with => @visitor[:password_confirmation]
  click_button "Sign up"
  find_user
end

def sign_in
  visit '/users/sign_in'
  fill_in "Email", :with => @visitor[:email]
  fill_in "Password", :with => @visitor[:password]
  click_button "Log in"
end

### GIVEN ###
Given /^I am not logged in$/ do
  visit '/users/sign_out'
end

Given /^I am logged in$/ do
  create_user
  sign_in
end

Given /^I exist as a user$/ do
  create_user
end

Given /^I do not exist as a user$/ do
  create_visitor
  delete_user
end

Given /^I exist as an unconfirmed user$/ do
  create_unconfirmed_user
end

### WHEN ###
When /^I sign in with valid credentials$/ do
  create_visitor
  sign_in
end

When /^I sign out$/ do
  visit '/users/sign_out'
end

When /^I sign up with valid user data$/ do
  create_visitor
  sign_up
end

When /^I sign up with an invalid email$/ do
  create_visitor
  @visitor = @visitor.merge(:email => "notanemail")
  sign_up
end

When /^I sign up without a password confirmation$/ do
  create_visitor
  @visitor = @visitor.merge(:password_confirmation => "")
  sign_up
end

When /^I sign up without a password$/ do
  create_visitor
  @visitor = @visitor.merge(:password => "")
  sign_up
end

When /^I sign up with a mismatched password confirmation$/ do
  create_visitor
  @visitor = @visitor.merge(:password_confirmation => "changeme123")
  sign_up
end

When /^I return to the site$/ do
  visit '/'
end

When /^I sign in with a wrong email$/ do
  @visitor = @visitor.merge(:email => "wrong@example.com")
  sign_in
end

When /^I sign in with a wrong password$/ do
  @visitor = @visitor.merge(:password => "wrongpass")
  sign_in
end

When /^I edit my account details$/ do
  click_link "Edit account"
  fill_in "Email", :with => "newemail@email.com"
  fill_in "user_current_password", :with => @visitor[:password]
  click_button "Update"
end

When /^I look at the list of users$/ do
  visit '/'
end

When /^I look at the home page$/ do
  visit '/'
end

### THEN ###
Then /^I should be signed in$/ do
  page.should have_content "Logout"
  page.should_not have_content "Sign up"
  page.should_not have_content "Login"
end

Then /^I should be signed out$/ do
  page.should have_content "Sign up"
  page.should have_content "Login"
  page.should_not have_content "Logout"
end

Then /^I see an unconfirmed account message$/ do
  page.should have_content "You have to confirm your account before continuing."
end

Then /^I see a successful sign in message$/ do
  page.should have_content "Signed in successfully."
end

Then /^I should see a successful sign up message$/ do
  page.should have_content "Welcome! You have signed up successfully."
end

Then /^I should see an invalid email message$/ do
  page.should have_content "Email is invalid"
end

Then /^I should see a missing password message$/ do
  page.should have_content "Password can't be blank"
end

Then /^I should see a missing password confirmation message$/ do
  page.should have_content "Password confirmation doesn't match"
end

Then /^I should see a mismatched password message$/ do
  page.should have_content "Password confirmation doesn't match"
end

Then /^I should see a signed out message$/ do
  page.should have_content "Signed out successfully."
end

Then /^I see an invalid login message$/ do
  page.should have_content "Invalid email or password."
end

Then /^I should see an account edited message$/ do
  page.should have_content "You updated your account successfully."
end

Then /^I should see my name$/ do
  create_user
  page.should have_content @user[:name]
end

Then /^I should see the marketing message$/ do
  create_user
  page.should have_content "Train your BODY Demigod encourages you to improve your health, fitness, and nutrition through challenges that help you build healthy habits and create the lifestyle you want. SIGN UP Train your BRAIN Demigod provides challenges that motivate you to keep your brain active, whether it is reading a book, doing a puzzle, or trying a new language. SIGN UP open your mind Nutritional Keep your body healthy and follow a better diet thanks to several interesting challenges... more Physical Improve your physical strength and endurance and combine both through different exercises... more Intellectual Exercise your brain by challenging it with a wide range of activities... more Copyright 2014 All rights reserved Demigod.me"
end

Then /^I should see the statistics$/ do
  page.should have_content "Strength"
  page.should have_content "Intelligence"
  page.should have_content "Diet"
  page.should have_content "Endurance"
  page.should have_content "Health"
  page.should have_content "Altruism"
end

Then /^I should be urged to take a challenge$/ do
  page.should have_content "You are not currently taking a challenge! Click the button below to find one! Pick a challenge!"
end

Then /^I should be able to click the bottom button to go to challenges$/ do
  click_on "Pick a challenge!"
end

Then /^I should be able to click challenges on the navbar$/ do
  click_on "Challenges"
end

Then /^I should see various challenges$/ do
  page.should have_content "Physical"
  page.should have_content "Nutritional"
  page.should have_content "Intellectual"
  page.should have_content "Name"
  page.should have_content "Description"
  page.should have_content "Points"
  page.should have_content "View All"
end
