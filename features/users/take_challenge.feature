Feature: User takes a challenge
  As a user
  I should be able to take a challenge, succeed in completing it, and level up for it
  so that I can better myself as a person

  Scenario: User should be told they are not taking a challenge if they are not yet taking one
  Given I am logged in
  When I look at the home page
  Then I should be urged to take a challenge

  Scenario: User should be able to view the challenges that exist
  Given I am logged in
  When I look at the home page
  Then I should be urged to take a challenge
  And I should be able to click the bottom button to go to challenges
  And I should see various challenges

  Scenario: User should be able to navigate to the challenges from navbar
  Given I am logged in
  When I look at the home page
  Then I should be able to click challenges on the navbar
  And I should see various challenges
