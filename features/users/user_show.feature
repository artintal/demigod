Feature: Show Users
  As a visitor to the website
  I should see the marketing message if I am not logged in and the user profile page if I am
  so I can know how to use the application, or so I can use it

    Scenario: Viewing Marketing page
      Given I exist as a user
      When I look at the home page
      Then I should see the marketing message 
    
    Scenario: Looking at the profile page when logged in and not taking a challenge
      Given I am logged in
      When I look at the home page
      Then I should see the statistics
      And I should be urged to take a challenge
