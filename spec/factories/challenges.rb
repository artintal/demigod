# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :challenge do
    parent_id 1
    user_made false
    name "MyString"
    description "MyText"
    points 1
    category "MyString"
  end
end
