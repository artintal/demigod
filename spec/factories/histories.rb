# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :history do
    user_id 1
    user_challenge_id 1
    progress 1
  end
end
