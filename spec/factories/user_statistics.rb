# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user_statistic do
    user_id 1
    level 1
    points_until_level_up 1
    statistic_id 1
  end
end
