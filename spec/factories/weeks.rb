# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :week do
    start_week "2014-03-22 09:42:24"
    end_week "2014-03-22 09:42:24"
    checkins_completed false
  end
end
